﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using List.Models;

namespace List.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        readonly List<Item> items;

        public MockDataStore()
        {
            items = new List<Item>()
            {
                new Item { Id = Guid.NewGuid().ToString(), Name = "France",
                    Description="France is the largest country in the European Union and the second largest in Europe. It has been one of the world's foremost powers for many centuries. During the 17th and 18th centuries, France colonized much of North America; during the 19th and early 20th centuries, France built one of the largest colonial empires of the time, including large portions of North, West and Central Africa, Southeast Asia, and many Pacific islands. France is a developed country and possesses the fifth largest economy[11] in the world, according to nominal GDP figures. It is the most visited country in the world, receiving 82 million foreign tourists annually.[12] France is one of the founding members of the European Union, and has the largest land area of all members. It is also a founding member of the United Nations, and a member of the Francophonie, the G8, NATO, and the Latin Union. It is one of the five permanent members of the United Nations Security Council and owns the largest number of nuclear weapons with active warheads and nuclear power plants in the European Union.",
                    Image = "https://upload.wikimedia.org/wikipedia/commons/c/c1/Flag_map_of_Greater_France.png", },
                new Item { Id = Guid.NewGuid().ToString(), Name = "Germany",
                    Description="Germany includes 16 constituent states, covers an area of 357,386 square kilometres (137,988 sq mi),[7] and has a largely temperate seasonal climate. With 83 million inhabitants, it is the second most populous state of Europe after Russia, the most populous state lying entirely in Europe, as well as the most populous member state of the European Union. Germany is a very decentralised country. Its capital and largest metropolis is Berlin, while Frankfurt serves as its financial capital and has the country's busiest airport.",
                    Image="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Flag_map_of_Germany.svg/1200px-Flag_map_of_Germany.svg.png", },
                new Item { Id = Guid.NewGuid().ToString(), Name = "United State",
                    Description="The United States of America (USA), commonly known as the United States (U.S. or US) or America, is a country consisting of 50 states, a federal district, five major self-governing territories, and various possessions.[g] At 3.8 million square miles (9.8 million km2), it is the world's third or fourth-largest country by total area[b] and is slightly smaller than the entire continent of Europe. Most of the country is located in central North America between Canada and Mexico. With an estimated population of over 328 million, the U.S. is the third most populous country in the world. The capital is Washington, D.C., and the most populous city is New York City.",
                    Image="https://clipart.info/images/ccovers/1559854952usa-flag-map-png-2.png", },
                new Item { Id = Guid.NewGuid().ToString(), Name = "Cameroon",
                    Description="Cameroon (/ˌkæməˈruːn/ (About this soundlisten); French: Cameroun), officially the Republic of Cameroon (French: République du Cameroun), is a country in Central Africa. It is bordered by Nigeria to the west and north; Chad to the northeast; the Central African Republic to the east; and Equatorial Guinea, Gabon and the Republic of the Congo to the south. Cameroon's coastline lies on the Bight of Biafra, part of the Gulf of Guinea and the Atlantic Ocean. Although Cameroon is not an ECOWAS member state, it is geographically and historically in West Africa with the Southern Cameroons which now form her Northwest and Southwest Regions having a strong West African history. The country is sometimes identified as West African and other times as Central African due to its strategic position at the crossroads between West and Central Africa. Cameroon is home to over 250 native languages spoken by nearly 20 million people.[8][9] The official languages of the country are English and French.",
                    Image="https://cdn.imgbin.com/11/23/18/imgbin-flag-of-cameroon-map-wikimedia-commons-road-map-infography-aerial-view-ve0PWiVKw4qscPJBHzTA68BNL.jpg"},
                new Item { Id = Guid.NewGuid().ToString(), Name = "China",
                    Description="China emerged as one of the world's first civilizations, in the fertile basin of the Yellow River in the North China Plain. For millennia, China's political system was based on hereditary monarchies, or dynasties, beginning with the semi-mythical Xia dynasty in 21st century BCE. Since then, China has expanded, fractured, and re-unified numerous times. In the 3rd century BCE, the Qin reunited core China and established the first Chinese empire. The succeeding Han dynasty, which ruled from 206 BCE until 220 CE, saw some of the most advanced technology at that time, including papermaking and the compass, along with agricultural and medical improvements. The invention of gunpowder and movable type in the Tang dynasty (618–907) and Northern Song (960–1127) completed the Four Great Inventions. Tang culture spread widely in Asia, as the new Silk Route brought traders to as far as Mesopotamia and the Horn of Africa. Dynastic rule ended in 1912 with the Xinhai Revolution, when the Republic of China (ROC) replaced the Qing dynasty. China, as a whole, was ravaged by feudal warlordism and Japan during World War II. The subsequent Chinese Civil War resulted in a division of territory in 1949 when the Communist Party of China led by Mao Zedong established the People's Republic of China on mainland China while the Kuomintang-led nationalist government retreated to the island of Taiwan where it governed until 1996 when Taiwan transitioned to democracy. The political status of Taiwan remains disputed to this day.",
                    Image="https://pakamstudies.files.wordpress.com/2011/01/china-flag-map.png?w=800"},
                new Item { Id = Guid.NewGuid().ToString(),
                    Name = "Japan",
                    Description="Japan (Japanese: 日本, Nippon [ɲippoꜜɴ] (About this soundlisten) or Nihon [ɲihoꜜɴ] (About this soundlisten); officially 日本国, About this soundNippon-koku or Nihon-koku) is an island country located in East Asia. It is bordered by the Sea of Japan to the west and the Pacific Ocean to the east, and spans more than 3,000 kilometers (1,900 mi) along the coast of the continent from the Sea of Okhotsk in the north to the East China Sea and Philippine Sea in the south. Part of the Pacific Ring of Fire, Japan encompasses a stratovolcanic archipelago of about 6,852 islands, with five main islands (Hokkaido, Honshu, Kyushu, Shikoku, and Okinawa) comprising 97% of the country's total area of 377,975 square kilometers (145,937 sq mi).",
                    Image="https://www.seekpng.com/png/detail/82-821375_japan-map-of-japan-with-flag.png"}
            };
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}